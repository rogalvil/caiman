import React, { Component } from 'react';
import { BrowserRouter as Router, Link, Redirect, Route, Switch } from 'react-router-dom'
import Home from './components/Home'
import AppForm from './components/AppForm'
import IndustriesUploadForm from './components/IndustriesUploadForm'
import IndustryShowForm from './components/IndustryShowForm'
import CheckoutForm from './components/CheckoutForm'

class App extends Component {

  constructor() {
    super();
    this.state = {
    }
  }

  render() {
    return (
      <Router>
        <div className="App">
          <div className="nav">
            <Link to="/apps/register">Register App</Link>

            <Link to="/industries/upload">Industries Upload</Link>
            <Link to="/industries">Industries Show</Link>
            <Link to="/checkout">Payment</Link>
          </div>
          <Route exact path="/" render={()=><Home />} />
          <Route exact path="/apps/register" render={()=><AppForm />} />
          <Route exact path="/industries/upload" render={()=><IndustriesUploadForm />} />
          <Route exact path="/industries" render={()=><IndustryShowForm />} />
          <Route exact path="/checkout" render={()=><CheckoutForm />} />
        </div>
      </Router>
    );
  }
}

export default App;
