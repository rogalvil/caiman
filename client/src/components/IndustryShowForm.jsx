import React, { Component } from 'react'
import IndustryItem from './IndustryItem'

class IndustryShowForm extends Component {
  constructor() {
    super();
    this.state = {
      id: '',
      token: '',
      industriesList: null,
      industriesListLoaded: false,
      errors: [],
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleAppSubmit =  this.handleAppSubmit.bind(this);

  }

  handleChange(e) {
    const name = e.target.name;
    const val = e.target.value;
    this.setState({
      [name]: val,
    })
  }


  handleAppSubmit(e, data) {
    e.preventDefault();

    fetch('/industries/'+data.id, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Token '+data.token,
      }
    }).then(res => res.json())
      .then(res => {
        console.log(res)
        this.setState({
          industriesList: res.industries,
          industriesListLoaded: true,
          errors: res.errors,
        })
      })
      .catch(err => {
        console.log(err)
      })
  }

  render() {
    return (
      <div className="industries">
        <div>Consulta las industrias por su id. Se require el token obtenido en el registro de app. </div>

        <div className="form">
          <form onSubmit={(e)=> this.handleAppSubmit(e, this.state)} >
            <input type="text" name="id" placeholder="id" value={this.state.id} onChange={this.handleChange} />
            <input type="text" name="token" placeholder="token" value={this.state.token} onChange={this.handleChange} />
            <input type="submit" value="Solicitar" />
          </form>

        </div>
        <div className="industries-list">
        {(this.state.errors)
          ? <p>{this.state.errors.map(e=><div>{e}</div>)}</p>
          : <p>{this.renderIndustries()}</p> }

        </div>
      </div>
    )
  }

  renderIndustries() {
    return (
      <IndustryItem children={this.state.industriesList} index={0} />
    )
  }

}

export default IndustryShowForm;