import React, { Component } from 'react'

export default class IndustryItem extends Component {

  render() {

    const { children, index } = this.props

    let spaces = []
    for (let i = 0; i < index; ++i) {
      spaces.push(<span key={i}>&nbsp;</span>)
    }
    
    return (
      <div className="industries">
        {children.map(item =>
          <div key={item.id} className="industry">
            { spaces }
            <span>{item.id}</span> &nbsp;
            <span>{item.name}</span>
            {item.children && <IndustryItem children={item.children} index={index + 1}/>}
          </div>
        )}
      </div>
    )

  }

}


