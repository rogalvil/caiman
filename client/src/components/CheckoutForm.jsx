import React, { Component } from 'react'

class CheckoutForm extends Component {
  constructor() {
    super();
    this.state = {
      amount: '',
      card_number: '',
      fullname: '',
      card_type: '',
      expirate_date_month: '',
      expirate_date_year: '',
      cvv: '',
      transactionLoaded: false
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleAppSubmit =  this.handleAppSubmit.bind(this);

  }

  handleChange(e) {
    const name = e.target.name;
    const val = e.target.value;

    this.setState({
      [name]: val,
    })
  }


  handleAppSubmit(e, data) {
    e.preventDefault();
    console.log(data)
    fetch('/checkout', {
      method: 'POST',
      body: JSON.stringify({
        transaction: data
      }),
      headers: {
        'Content-Type': 'application/json',
      }
    }).then(res => res.json())
      .then(res => {
        console.log(res)
        this.setState({
          code: res.code,
          errors: res.errors
        })
      })
      .catch(err => {
        console.log(err)
      })
  }

   render() {
    return (
      <div className="checkout">
        <div>Captura el monto y tus datos de pago</div>

        <div className="form">
          <form onSubmit={(e)=> this.handleAppSubmit(e, this.state)} >
            <label>Monto: </label>
            <input type="text" name="amount" placeholder="Monto" value={this.state.amount} onChange={this.handleChange} />
            <br />
            <label>Número de tarjeta: </label>
            <input type="text" name="card_number" placeholder="Número de tarjeta" value={this.state.card_number} onChange={this.handleChange} />
            <br />
            <label>Nombre completo: </label>
            <input type="text" name="fullname" placeholder="Nombre completo" value={this.state.fullname} onChange={this.handleChange} />
            <br />
            <label>Tipo de tarjeta: </label>
            <select name="card_type" value={this.state.card_type} onChange={this.handleChange} >
              <option value="" disabled selected>Tipo de tarjeta...</option>
              <option value="credit">Credito</option>
              <option value="debit">Debito</option>
            </select>
            <br />
            <label>Fecha de expiración: </label>
            <select name="expirate_date_month" value={this.state.expirate_date_month} onChange={this.handleChange} >
              <option value="" disabled selected>Mes...</option>
              <option value="01">Enero</option>
              <option value="02">Febrero</option>
              <option value="03">Marzo</option>
              <option value="04">Abril</option>
              <option value="05">Mayo</option>
              <option value="06">Junio</option>
              <option value="07">Julio</option>
              <option value="08">Agosto</option>
              <option value="09">Septiembre</option>
              <option value="10">Octubre</option>
              <option value="11">Noviembre</option>
              <option value="12">Diciembre</option>
            </select>
            /
            <select name="expirate_date_year" value={this.state.expirate_date_year} onChange={this.handleChange} >
              <option value="" disabled selected>Año...</option>
              <option value="18">2018</option>
              <option value="19">2019</option>
              <option value="20">2020</option>
              <option value="21">2021</option>
              <option value="22">2022</option>
              <option value="23">2023</option>
              <option value="24">2024</option>
              <option value="25">2025</option>
              <option value="26">2026</option>
              <option value="27">2027</option>
              <option value="28">2028</option>
              <option value="29">2029</option>
              <option value="30">2030</option>
              <option value="31">2031</option>
            </select>
            <br />
            <input type="text" name="cvv" placeholder="Código de seguridad" value={this.state.cvv} onChange={this.handleChange} />
            <br />
            <input type="submit" value="Pagar" />
          </form>

        </div>
        <div className="checkout-response">
        {(this.state.errors)
          ? <p>{this.state.errors.map(e=><div>{e}</div>)}</p>
          : <p>Su pago se proceso su identificador de operación es: {this.state.code}</p> }
        </div>
      </div>
    )
  }



}

export default CheckoutForm;