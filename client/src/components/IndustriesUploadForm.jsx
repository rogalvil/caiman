import React, { Component } from 'react'
import IndustryItem from './IndustryItem'

class IndustriesUploadForm extends Component {
  constructor() {
    super();
    this.state = {
      file: '',
      industriesList: null,
      industriesListLoaded: false,
      errors: [],
    }
    this.handleChangeFile = this.handleChangeFile.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleAppSubmit =  this.handleAppSubmit.bind(this);

  }

  handleChange(e) {
    const name = e.target.name;
    const val = e.target.value;
    this.setState({
      [name]: val,
    })
  }

  handleChangeFile(e) {
    this.setState({
      file: e.target.files[0]
    })
  }

  handleAppSubmit(e, data) {
    e.preventDefault();
    console.log(data)

    let formData = new FormData();
    formData.append('industries[file]', this.state.file);

    let options = {
      method: 'POST',
      headers: {
        'Authorization': 'Token '+data.token,

      },
      body: formData
    }

    fetch('/industries/upload', options)
      .then(res => res.json())
      .then(res => {
        console.log(res.industries)
        this.setState({
          industriesList: res.industries,
          industriesListLoaded: true,
          errors: res.errors,
        })
      })
      .catch(err => {
        console.log(err)
      })

  }

  render() {
    return (
      <div className="industries">
        <div>Sube un archivo CSV con columnas id y name, para almacenar las industrias. Se require el token obtenido en el registro de app. </div>

        <div className="form">
          <form onSubmit={(e)=> this.handleAppSubmit(e, this.state)} >
            <input type="file" name="file" placeholder="file" onChange={this.handleChangeFile} />
            <input type="text" name="token" placeholder="token" value={this.state.token} onChange={this.handleChange} />
            <input type="submit" value="Enviar" />
          </form>

        </div>
        <div className="industries-list">
        {(this.state.errors)
          ? <p>{this.state.errors.map(e=><div>{e}</div>)}</p>
          : <p>{this.renderIndustries()}</p> }

        </div>
      </div>
    )
  }

  renderIndustries() {
    return (
      <IndustryItem children={this.state.industriesList} index={0} />
    )
  }

}

export default IndustriesUploadForm;