import React, { Component } from 'react'

class AppForm extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      description: '',
      token: '',
      registerLoaded: false,
      errors: []
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleAppSubmit =  this.handleAppSubmit.bind(this);

  }

  handleChange(e) {
    const name = e.target.name;
    const val = e.target.value;
    this.setState({
      [name]: val,
    })
  }

  handleAppSubmit(e, data) {
    e.preventDefault();
    console.log(data)
    fetch('/apps/register', {
      method: 'POST',
      body: JSON.stringify({
        app: data
      }),
      headers: {
        'Content-Type': 'application/json',
      }
    }).then(res => res.json())
      .then(res => {
        console.log(res)
        this.setState({
          token: res.token,
          registerLoaded: res.token !== undefined,
          errors: res.errors
        })
      })
      .catch(err => {
        console.log(err)
      })
  }

  render() {
    return (
      <div className="register">
        <div>Registra tu app y obten un token para registrar las industrias o consultarlas </div>
        <div className="form">
          <form onSubmit={(e)=> this.handleAppSubmit(e, this.state)} >
            <input type="text" name="name" placeholder="name" value={this.state.name} onChange={this.handleChange} />
            <input type="text" name="description" placeholder="description" value={this.state.description} onChange={this.handleChange} />
            <input type="submit" value="Enviar" />
          </form>
        </div>
        <div className="token">
        {(this.state.errors)
          ? <p>{this.state.errors.map(e=><div>{e}</div>)}</p>
          : <p>Token: {this.state.token}</p> }
        </div>
      </div>
    )
  }



}

export default AppForm;