class Transaction < ApplicationRecord
  before_create :generate_code
  before_save :data_prepare
  before_validation :data_decrypt
  CARD_TYPES = %w(credit debit)

  validates_presence_of :card_number, :fullname, :card_type, :expirate_date, :cvv
  validates :card_type, inclusion: { in: CARD_TYPES }
  validates :card_number, numericality: true, length: { is: 16 }
  validates :expirate_date, length: { is: 5 }
  validates :cvv, length: { is: 3 }

  attr_accessor   :expirate_date_month
  attr_accessor   :expirate_date_year

  private
    def generate_code
      self.code = loop do
        random_token = SecureRandom.urlsafe_base64(nil, false)
        break random_token unless Transaction.exists?(code: random_token)
      end
    end

    def data_prepare
      puts "data prepare\n"
      puts "#{self.expirate_date_month}/#{self.expirate_date_year}\n"
      self.last_digits = self.card_number.last(4)
      self.card_number = Cipher::AES.encrypt(self.card_number)
      self.fullname = Cipher::AES.encrypt(self.fullname)
      self.expirate_date = Cipher::AES.encrypt(self.expirate_date)
      self.cvv = Cipher::AES.encrypt(self.cvv)
      
      self.status = "processing"
    end

    def data_decrypt
      puts persisted?
      puts self.attribute_present?(:card_number)
      puts self.attribute_present?(:fullname)
      puts self.attribute_present?(:cvv)
      puts self.attribute_present?(:expirate_date)
      puts "#{self.expirate_date_month}/#{self.expirate_date_year}\n"
      self.expirate_date = "#{self.expirate_date_month}/#{self.expirate_date_year}"
      if persisted?
        self.card_number = Cipher::AES.decrypt(self.card_number) if self.attribute_present?(:card_number)
        self.fullname = Cipher::AES.decrypt(self.fullname) if self.attribute_present?(:fullname)
        self.cvv = Cipher::AES.decrypt(self.cvv)  if self.attribute_present?(:cvv)
        self.expirate_date = Cipher::AES.decrypt(self.expirate_date)  if self.attribute_present?(:expirate_date)
      end
    end

end
