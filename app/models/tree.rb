class Tree
  attr_reader :array, :tree

  def initialize(array, il = 1)
    @array = array
    @il =  il
    create_tree!
  end

  private

  def create_tree!
    @tree = []
    array.each do |hash|
      parent = hash["id"].chop
      data = hash
      data = data.merge({parent: parent}) if array.any? {|h| h["id"] == parent}
      id = hash["id"]
      path = id.split('')
      path = [id[0, @il]] + (path.drop(@il))
      process_node(path, data)
    end
  end

  def process_node(path, data)
    current = @tree
    path.each.with_index do |node, index|
      if persist = current.find { |hash| hash[:id] == path.take(index + 1).join('') }
        current = persist[:children]
      else
        current << {
          id: data["id"],
          name: data["name"],
          children: []
        }
        current = current.last[:children]
      end
    end
  end


end