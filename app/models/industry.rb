class Industry < ApplicationRecord
  require 'csv'

  def self.import(file)
    industries_array = []
    CSV.foreach(file.path, headers: true) do |row|
      industry_hash = row.to_hash
      industries_array << industry_hash.clone
      industry_hash['key'] = industry_hash.delete('id')
      industries = Industry.where(key: industry_hash["key"] )
      if industries.count == 1
        industries.first.update_attributes(industry_hash)
      else
        Industry.create(industry_hash)
      end
    end
    { industries: Tree.new(industries_array).tree }
  end



end
