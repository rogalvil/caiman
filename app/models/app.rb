class App < ApplicationRecord
  validates :name, uniqueness: true

  has_secure_token

end
