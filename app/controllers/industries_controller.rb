class IndustriesController < ApiController

  before_action :require_login

  def create
    industries_hash = Industry.import params[:industries][:file].tempfile
    render json: industries_hash
  end

  def show
    industries = Industry.select(:key, :name).where("key LIKE ?", "#{params[:id]}%").order(key: "asc")
    industries_array = industries.to_a.map(&:serializable_hash)
    industries_array = industries_array.map{|h| {"id"=>h['key'], "name"=>h['name']} }
    render json: { industries: Tree.new(industries_array, params[:id].length).tree }
  end

end
