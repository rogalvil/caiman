class AppsController < ApiController
  skip_before_action :require_login, only: [:create], raise: false

  def create
    @app = App.new(app_params)
    if @app.save
      render json: {name: @app.name, token: @app.token}, status: :created
    else
      render json: {errors: @app.errors.full_messages}, status: :unprocessable_entity
    end
  end

  private
    def set_app
      @app = App.find(params[:id])
    end

    def app_params
      params.require(:app).permit(:name, :description)
    end

end
