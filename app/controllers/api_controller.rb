class ApiController < ApplicationController
  def require_login
    authenticate_token || unauthorized("No autorizado")
  end

  def current_app
    @current_app ||= authenticate_token
  end

  protected
    def unauthorized(message)
      errors =  { errors: [message] }
      render json: errors, status: :unauthorized
    end

    def authenticate_token
      token = request.headers[:token]
      #print token
      authenticate_with_http_token do |token, options|
        App.find_by(token: token)
      end
    end

end
