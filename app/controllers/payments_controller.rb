class PaymentsController < ApiController

  def checkout
    @transaction = Transaction.new(transaction_params)
    if @transaction.save
      Sidekiq::Client.push('queue' => 'exq', 'class' => 'ProcessPaymentWorker', 'args' => [@transaction.id])

      render json: {last_digits: @transaction.last_digits, amount: @transaction.amount, code: @transaction.code}, status: :created
    else
      render json: {errors: @transaction.errors.full_messages}, status: :unprocessable_entity
    end
  end


  private
    def set_transaction
      @transaction = Transaction.find(params[:id])
    end

    def transaction_params
      params.require(:transaction).permit(:amount, :card_number, :fullname, :card_type, :expirate_date_month, :expirate_date_year, :cvv)
    end

end
