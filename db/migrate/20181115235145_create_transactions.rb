class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.float :amount
      t.string :card_number
      t.string :fullname
      t.string :card_type
      t.string :expirate_date
      t.string :cvv
      t.string :last_digits
      t.string :status
      t.string :authorization_code

      t.timestamps
    end
  end
end
