class AddKeyToIndustries < ActiveRecord::Migration[5.2]
  def change
    add_column :industries, :key, :string
  end
end
