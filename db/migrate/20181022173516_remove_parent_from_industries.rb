class RemoveParentFromIndustries < ActiveRecord::Migration[5.2]
  def change
    remove_column :industries, :parent, :string
  end
end
