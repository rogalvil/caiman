Sidekiq::Extensions.enable_delay!

if ENV["REDISCLOUD_URL"]
  $redis = Redis.new(url:  ENV["REDISCLOUD_URL"])
end


redis_options = {
  size: 4,
  namespace: 'exq'
}

Sidekiq.configure_client do |config|
  config.redis = redis_options
end
Sidekiq.configure_server do |config|
  #no redis settings
  config.redis = redis_options
end
