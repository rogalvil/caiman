Rails.application.routes.draw do

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sq'

  post 'apps/register' => 'apps#create'

  post 'industries/upload' => 'industries#create'
  get 'industries' => 'industries#index'
  get 'industries/:id' => 'industries#show'

  post 'checkout' => 'payments#checkout'


  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
